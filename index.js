// var TelegramBot = require('telegraf');
// var token = '1273835757:AAGmvH8GoQVWc4SlIvjM6f_DsdGWwpx6c1o';
// var bot = new TelegramBot(token,{polling:true});

// bot.onText(/\/echo (.+)/,function(msg,match) {
//     var chatId = msg.chat.id;
//     var echo = match[1];
//     bot.sendMessage(chatId,echo);
// });

const { Telegraf } = require('telegraf')
const Keyboard = require('telegraf-keyboard');
const { Router, Markup } = Telegraf

const bot = new Telegraf('1273835757:AAGmvH8GoQVWc4SlIvjM6f_DsdGWwpx6c1o');
bot.start((ctx) => {
    ctx.telegram.sendMessage(ctx.chat.id,'Welcome to TicTacToe Game!',
    {
        reply_markup:{
            inline_keyboard:[
                [{text:'PLAY!',callback_data:'play'}]
            ]
        }
    })
})
var board = []
var gameEnded=false
var positions=[]
const corners = ['0,0','0,2','2,0','2,2']
const others = ['0,1','1,0','1,2','2,1']
const xarr = ['0,0','0,1','0,2','1,0','1,1','1,2','2,0','2,1','2,2']

bot.action('play', (ctx) => {
    board = [
        ['','',''],
        ['','',''],
        ['','','']
    ]
    gameEnded=false
    positions = []
    ctx.deleteMessage()
    ctx.telegram.sendMessage(ctx.chat.id,'Choose The Side:',
    {
        reply_markup:{
            inline_keyboard:[
                [{text:'❌',callback_data:'x'},{text:'⭕️',callback_data:'o'}]
            ]
        }
    })
})

bot.action('x', (ctx) => {
    ctx.deleteMessage()
    ctx.telegram.sendMessage(ctx.chat.id,'You Choosed:❌',
    {
        reply_markup:{
            inline_keyboard:[
                [{text:'◻️',callback_data:'0,0'},{text:'◻️',callback_data:'0,1'},{text:'◻️',callback_data:'0,2'}],
                [{text:'◻️',callback_data:'1,0'},{text:'◻️',callback_data:'1,1'},{text:'◻️',callback_data:'1,2'}],
                [{text:'◻️',callback_data:'2,0'},{text:'◻️',callback_data:'2,1'},{text:'◻️',callback_data:'2,2'}]
            ]
        }
    })
})
bot.action('o', (ctx) => {
    ctx.deleteMessage()
    ctx.telegram.sendMessage(ctx.chat.id,'You Choosed:⭕️',
    {
        reply_markup:{
            inline_keyboard:[
                [{text:'◻️',callback_data:'1'},{text:'◻️',callback_data:'2'},{text:'◻️',callback_data:'3'}],
                [{text:'◻️',callback_data:'4'},{text:'◻️',callback_data:'5'},{text:'◻️',callback_data:'6'}],
                [{text:'◻️',callback_data:'7'},{text:'◻️',callback_data:'8'},{text:'◻️',callback_data:'9'}]
            ]
        }
    })
})


bot.action(xarr, async (ctx) => {
    ctx.deleteMessage()

    arr = ctx.callbackQuery.message.reply_markup.inline_keyboard

    position = ctx.callbackQuery.data
    p = position.split(",")
    arr[p[0]][p[1]].text='❌'
    board[p[0]][p[1]]='x'
    if(isWinner(board,'x')){
        gameEnded=true
        console.log("X WINS")
        ctx.deleteMessage()
        ctx.reply('Congrats! You Win!')
    }else{
        console.log('No one wins')
        if(isBoardFull(board)){
            gameEnded=true
            ctx.deleteMessage()
            ctx.reply('It is a TIE')
        }
    }

    if(!gameEnded){
        
        if(!isPosFree(position)){
            positions.push(position)
            o = await opponentsMove(positions,board).catch(() => {})
            console.log(typeof(o))
            console.log(o)
            positions.push(o.join(','))
            console.log('pos: ',positions)
            console.log('ctx: ',arr)
            arr[o[0]][o[1]].text='⭕️'
            board[o[0]][o[1]]='o'
            if(isWinner(board,'o')){
                console.log("O WINS")
                gameEnded=true
                ctx.deleteMessage()
                ctx.reply('Sorry, You Lose!')
            }else{
                if(isBoardFull(board)){
                    gameEnded=true
                    ctx.deleteMessage()
                    ctx.reply('It is a TIE')
                }
            }
        }
        if(!gameEnded){
            ctx.telegram.sendMessage(ctx.chat.id,'Choose Next Location:',
            {
                reply_markup:{
                    inline_keyboard:arr
                }
            })
        }
    }

})
function isPosFree(position){
    return positions.includes(position);
}
async function opponentsMove(positions,board) {
    // for(var i = 0; i < board.length; i++) {
    //     for(var j = 0; j < board[i].length; j++) {
    //         copy = copyBoard(board)
    //         if(isSpaceFree(copy, i,j)){
    //             makeMove(copy,'o',i,j)
    //             if(isWinner(copy,'o')){
    //                 arr = [i,j]
    //                 return arr
    //             }
    //         }
    //     }
    // }

    // await Promise.reject(new Error('test'));

    for(var i = 0; i < board.length; i++) {
        for(var j = 0; j < board[i].length; j++) {
            copy = copyBoard(board)
            if(isSpaceFree(copy, i,j)){
                makeMove(copy,'x',i,j)
                if(isWinner(copy,'x')){
                    console.log('blocking X')
                    arr=[i.toString(),j.toString()]
                    return arr
                }
            }
        }
    }
    // move = chooseRandomMoveFromList(corners,positions)
    // if(move!=undefined){
    //     return move
    // }
    // if(isSpaceFree(board,1,1)){
    //     return ['1,1']
    // }
    // return chooseRandomMoveFromList(others,positions)
    var x = Math.floor((Math.random() * 8) + 0);
    while(positions.includes(xarr[x])){
        x = Math.floor((Math.random() * 8) + 0);
    }
    return xarr[x].split(',')
}
function makeMove(board,side,row,col) {
    board[row][col]=side
    
}
function isSpaceFree(board,row,col) {
    return board[row][col] == ''
}
function chooseRandomMoveFromList(list,positions) {
    var x = Math.floor((Math.random() * 3) + 0);
    while(positions.includes(list[x])){
        x = Math.floor((Math.random() * 3) + 0);
    }
    return corners[x]

}
function isWinner(board,side) {
    return ((board[0][0] == side && board[0][1]== side && board[0][2]== side) ||
    (board[1][0]== side && board[1][1]== side && board[1][2]== side) || 
    (board[2][0]== side && board[2][1]== side && board[2][2]== side) || 
    (board[0][0]== side && board[1][0]== side && board[2][0]== side) || 
    (board[0][1]== side && board[1][1]== side && board[2][1]== side) || 
    (board[0][2]== side && board[1][2]== side && board[2][2]== side) || 
    (board[0][0]== side && board[1][1]== side && board[2][2]== side) || 
    (board[0][2]== side && board[1][1]== side && board[2][0]== side)) 
}
function copyBoard(board) {
    var copyOfBoard = [
        ['','',''],
        ['','',''],
        ['','','']
    ]
    for(var i = 0; i < board.length; i++) {
        for(var j = 0; j < board[i].length; j++) {
            copyOfBoard[i][j]=board[i][j];
        }
    }
    return copyOfBoard
}
function isBoardFull(board) {
    for(var i = 0; i < board.length; i++) {
        for(var j = 0; j < board[i].length; j++) {
            if(isSpaceFree(board,i,j)){
                return false
            }
        }
    }
    return true
}
bot.launch()

